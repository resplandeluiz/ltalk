    var express = require('express')
        , load = require('express-load')
        , app = express()
        , cookieParser = require('cookie-parser')
        , session = require('express-session')
        , bodyParser = require('body-parser')
        , error = require('./middleware/error')
        , http = require('http').createServer(app)
        , io = require('socket.io')(http)


    app.set('views', __dirname + '/views')
    app.set('view engine', 'ejs')

    app.use( cookieParser('ltalk') )
    app.use( session() )
    app.use( bodyParser.json() )
    app.use( bodyParser.urlencoded({extended: true} ))
    app.use( express.methodOverride() )
    app.use( app.router )
    app.use( express.static(__dirname + '/public' ))
    app.use(error.notFound)
    app.use(error.serverError)


    load('models').then('controllers').then('routes').into(app)

    io.sockets.on('connection', client => {
      client.on('send-server', data => {
        var msg = "<b>" + data.nome + ":</b> " + data.msg + "<br>"
        client.emit('send-client', msg)
        client.broadcast.emit('send-client', msg)
      })
    })

    http.listen(3000, function(){  console.log("Ltalk no ar.");  })

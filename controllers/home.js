
  module.exports = function(app) {

      var HomeController = {

        index: (req, res) => {   res.render('home/index')  },
        login: function(req, res) {

            var usuario = {
              "email": req.body.email,
              "nome": req.body.nome
            }

            if( usuario.email && usuario.nome) {

                usuario['contatos'] = []
                req.session.usuario = usuario
                res.redirect('/contatos')

            } else { res.redirect('/') }
        },

        logout: function(req, res) {

          req.session.destroy()
          res.redirect('/')
          
        }
      };

        return HomeController
  }
